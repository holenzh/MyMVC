package com.freedom.web.mymvc.view;

import com.freedom.web.mymvc.utils.ViewType;

public class ModelAndView {
	private ViewType type;
	private Object model;
	private String view;

	public ModelAndView(ViewType t, Object m, String v) throws Exception {
		this.type = t;
		this.model = m;
		this.view = v;
		if (null == model || null == view || view.trim().length() == 0) {
			throw new Exception("build ModelAndView failed...");
		}
	}

	public ViewType getType() {
		return type;
	}

	public Object getModel() {
		return model;
	}

	public String getView() {
		return view;
	}
}